
$j(document).ready(function() {



    if ($j('#EcomOrderDeliveryName').val() != "") {
        $j('#delivform').css('display', 'block');
        $j('#delivadd').attr('checked', 'checked');
        $j('#delivform input').removeAttr('disabled');
    }
    else {

        $j('#delivform').css('display', 'none');
        $j('#delivadd').attr('checked', false);
        $j('#delivform input').attr('disabled', 'disabled');

        //$j('#delivadd').reomveAttr('checked');


    }


    /*********************************
    * addMethod tilf�jer en valideringsregel til valideringen
    * Denne regel tjeker om et felt KUN indeholder bogstaver og tal.
    ***********************************/

    $j.validator.addMethod("textOnly",
            function(value, element) {
                return /^[a-zA-Z0-9������ ]+$/.test(value);
            },
            "Navnet m� kun indeholde bogstaver og tal."
        );
    $j.extend($j.validator.messages, {
        required: "Feltet skal udfyldes!"


    });
    /*********************************
    * Funktionen validerer brugerinput ifb. med checkud.
    ***********************************/

    $j("#submitUserData").validate({
        ignore: ":disabled",    //Hvis input feltet er disabled skal det ikke valideres(dette bruges hvis man ikke �nsker alternativ leveringsadresse
        rules: {                //Herefter f�lger reglerne for hvert inputfelt. Felterne findes p� deres NAME

            EcomOrderDeliveryName: {
                required: true,
                minlength: 2
            },

            EcomOrderDeliveryAddress: {
                required: true,
                minlength: 2
            },

            EcomOrderDeliveryCity: {
                required: true,
                minlength: 2
            },
            EcomOrderDeliveryZip: {
                required: true,
                rangelength: [4, 4],
                digits: true
            }


        },
        messages: {         //Nedenst�ende er fejlmeddelelser til hver regel p� hvert inputfelt.


            EcomOrderDeliveryName: {
                minlength: "Navn skal best� af mindst 2 karakterer"
            },

            EcomOrderDeliveryAddress: {
                minlength: "Adressen skal best� af mindst 2 karakterer"
            },
            EcomOrderDeliveryCity: {
                minlength: "Bynavnet skal best� af mindst 2 karakterer"
            },
            EcomOrderDeliveryZip: {
                rangelength: "Postnummeret skal best� af 4 tal",
                digits: "Postnummeret skal best� af 4 tal"
            }
        }

    });





    /**********************************************************
    * Validerer formularen ved submit med et a tag med 
    * klassen submit istedet for en submit knap. 
    ***********************************************************/
    $j("input.submit").click(function() {
        if ($j("#submitUserData").valid()) {
            document.getElementById('submitUserData').submit();
            //return true;
        }
        else {
            return false;
        }
    });


    /*********************************************************
    * Viser/skjuler inputfelterne til leveringsadresse. 
    * Samtidig disables alle input felter til leveringsadresse n�r de skjules. 
    * Dette g�res for at undg� validering - se ovenfor
    **********************************************************/
    $j("#delivadd").click(function() {

        if ($j('#delivadd').is(':checked')) {
            $j('#delivform input').removeAttr('disabled');
            $j('#delivform').css('display', 'block');

        }
        else {
            $j('#delivform').css('display', 'none');
            $j('#delivform input').attr('disabled', 'disabled');
            $j("#delivform input").each(function() {
                $j(this).val('');
            })
        }
    });
});
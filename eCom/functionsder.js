
$j(document).ready(function() {



if ($j('#EcomOrderDeliveryName').val() != "") {
        $j('#delivform input').removeAttr('disabled');
        $j('#delivadd').attr('checked', 'checked')
    }
    else {
        $j('#delivform input').attr('disabled', 'disabled');

    }


    /*********************************
    * addMethod tilføjer en valideringsregel til valideringen
    * Denne regel tjeker om et felt KUN indeholder bogstaver og tal.
    ***********************************/

    $j.validator.addMethod("textOnly",
            function(value, element) {
                return /^[a-zA-Z0-9æøåÆØÅ ]+$/.test(value);
            },
            "Navnet må kun indeholde bogstaver og tal."
        );
    $j.validator.addMethod("noClinicselected", function(value, element, arg){
        if($j("#NIQNoBonusPoints2:checked").length==0){ //Hvis Nej tak IKKE er udfyldt, tjek om id 10 er valgt
            return 10 != $j("#NIQOreftag").val();
        }
        return true; //ellers lad være med at validere

 }, "Value must not equal arg.");

   $j.extend($j.validator.messages, {
     required: "Feltet skal udfyldes!"


   });
    /*********************************
    * Funktionen validerer brugerinput ifb. med checkud.
    ***********************************/

    $j("#submitUserData").validate({
        ignore: ":disabled",    //Hvis input feltet er disabled skal det ikke valideres(dette bruges hvis man ikke ønsker alternativ leveringsadresse
        rules: {                //Herefter følger reglerne for hvert inputfelt. Felterne findes på deres NAME
        
        NIQOreftag:{
            noClinicselected:"10"
        },
        Password: {required:true,
            minlength: 6},
    Password2: {
      equalTo: "#Password"
    },


            EcomOrderDeliveryName: {
                required: true,
                minlength: 2

            },

            EcomOrderDeliveryAddress: {
                required: true,
                minlength: 2
            },

            EcomOrderDeliveryCity: {
                required: true,
                minlength: 2
            },
            EcomOrderDeliveryZip: {
                required: true,
                rangelength: [4, 4],
                digits: true
            },
            EcomOrderCustomerName: {
                required: true,
                minlength: 2
            },

            EcomOrderCustomerEmail: {
                required: true,
                email: true
            },
            EcomOrderCustomerAddress: {
                required: true,
                minlength: 2
            },

            EcomOrderCustomerCity: {
                required: true,
                minlength: 2
            },
            EcomOrderCustomerZip: {
                required: true,
                rangelength: [4, 4],
                digits: true
            },
			  EcomOrderCustomerPhone: {
                required: true,
                minlength: 8,
                digits: true
            }


        },
        messages: {         //Nedenstående er fejlmeddelelser til hver regel på hvert inputfelt.
           NIQOreftag :{noClinicselected: "V&aelig;lg en klinik!"},
        Username:{
        minlength: "Telefonnummeret skal best&aring; af mindst 8 tal"
        },
        Password: {
            minlength: "Passwordet skal best&aring; af mindst 6 karakterer"
        },
        Password2: {
            equalTo: "Passwordet er ikke det samme som ovenst&aring;ende"
        },

        EcomOrderDeliveryName: {
            minlength: "Navn skal best&aring; af mindst 2 karakterer"
        },
            EcomOrderDeliveryName: {
                minlength: "Navn skal best&aring; af mindst 2 karakterer"
            },
            EcomOrderDeliveryEmail: {
                email: "Indtast venligst en valid email adresse"
            },
            EcomOrderDeliveryAddress: {
                minlength: "Adressen skal best&aring; af mindst 2 karakterer"
            },
            EcomOrderDeliveryCity: {
                minlength: "Bynavnet skal best&aring; af mindst 2 karakterer"
            },
            EcomOrderDeliveryZip: {
                rangelength: "Postnummeret skal best&aring; af 4 tal",
                digits: "Postnummeret skal best&aring; af 4 tal"
            },
                        EcomOrderCustomerName: {

                minlength: "Brugernavn skal best&aring; af mindst 2 karakterer"
            },
            EcomOrderCustomerEmail: {
                email: "Indtast venligst en valid email adresse"
            },
            EcomOrderCustomerAddress: {
                minlength: "Adressen skal best&aring; af mindst 2 karakterer"
            },
            EcomOrderCustomerCity: {
                minlength: "Bynavnet skal best&aring; af mindst 2 karakterer"
            },
			 EcomOrderCustomerPhone: {
                minlength: "Telefonnummeret skal best&aring; af mindst 8 tal",
                digits: "Telefonnummeret skal best&aring; af mindst 8 tal"
            },
            EcomOrderCustomerZip: {
                rangelength: "Postnummeret skal best&aring; af 4 tal",
                digits: "Postnummeret skal best&aring; af 4 tal"
            }
           
        }

    });





    /**********************************************************
    * Validerer formularen ved submit med et a tag med
    * klassen submit istedet for en submit knap.
    ***********************************************************/
    $j("a.submit").click(function() {
        if($j('#bonuswrapper :checkbox:checked').length==0){
            alert("Du skal enten tilmelde dig et nyhedsbrev, eller sige nej tak til bonuspoint");
            return false;
        };
        if ($j("#submitUserData").valid()) {
            document.getElementById('submitUserData').submit();
            //return true;
        }
        else {
            return false;
        }
    });


    /*********************************************************
    * Viser/skjuler inputfelterne til leveringsadresse.
    * Samtidig disables alle input felter til leveringsadresse når de skjules.
    * Dette gøres for at undgå validering - se ovenfor
    **********************************************************/
    $j("#delivadd").click(function() {

        if ($j('#delivadd').is(':checked')) {
            $j('#delivform input').removeAttr('disabled');

        }
        else {
            $j('#delivform input').attr('disabled', 'disabled');

        }

    });

});
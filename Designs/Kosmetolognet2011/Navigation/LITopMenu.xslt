<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >

  <!--
  Description: ul/li based navigation. No features from admin implemented.
  Recommended settings:
  Fold out: True or False
  Upper menu: Dynamic or Static
  First level: > 0
  Last level: >= First level
  -->
  
  <xsl:output method="xml" omit-xml-declaration="yes" indent="yes"  encoding="utf-8" />
  <xsl:param name="html-content-type" />
  <xsl:template match="/NavigationTree">
    
    <xsl:if test="count(//Page) &gt; 0">
      <ul>
        <xsl:apply-templates select="Page">
          <xsl:with-param name="depth" select="1"/>
        </xsl:apply-templates>
      </ul>
    </xsl:if>

  </xsl:template>

  <xsl:template match="//Page">
    <xsl:param name="depth"/>
    <li>
      <a>
        <xsl:if test="@InPath='True'">
          <xsl:attribute name="class">inpath</xsl:attribute>
        </xsl:if>
        <xsl:if test="position() = 1">
          <xsl:attribute name="class">firstitem</xsl:attribute>
        </xsl:if>
        <xsl:if test="position() = count(//Page)">
          <xsl:attribute name="class">lastitem</xsl:attribute>
        </xsl:if>
        <xsl:if test="@Active='True'">
          <xsl:attribute name="id">activeitem</xsl:attribute>
        </xsl:if>
        <xsl:attribute name="href"><xsl:value-of select="@FriendlyHref" disable-output-escaping="yes"/></xsl:attribute>
        <xsl:value-of select="@MenuText" disable-output-escaping="yes"/>
      </a>
        
    </li>
  </xsl:template>

</xsl:stylesheet>
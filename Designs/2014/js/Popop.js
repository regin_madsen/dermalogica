var Popup = function ($) {
    var
		method = {},
        settings,
		$overlay,
		$modal,
		$content,
        timeoutHandler,
        $close;

    // Public properties
    method.isOpen = false;
    method.content = null;

    // Center the modal in the viewport
    method.center = function () {
        var top, left;

        //top = Math.max($(window).height() - $modal.outerHeight(), 0) / 2;
        //left = Math.max($(window).width() - $modal.outerWidth(), 0) / 2;

        //$modal.css({
        //	top:top + $(window).scrollTop(),
        //	left:left + $(window).scrollLeft()
        //});
        $modal.css({
            position: 'fixed',
            'top': '50%',
            'left': '50%',
            'margin-left': '-' + $modal.width() / 2 + 'px',
            'margin-top': '-' + $modal.height() / 2 + 'px'
        });


    };

    // Init
    method.init = function () {
        if ($overlay) return; // Allready loaded

        // Generate the HTML and add it to the document
        $overlay = $('<div id="modal-overlay"></div>');
        $modal = $('<div id="modal-box"></div>');
        $content = $('<div id="modal-content"></div>');
        $close = $('<a id="modal-close" href="#">close</a>');

        $modal.hide();
        $overlay.hide();
        $modal.append($content, $close);

        // Add close handler
        var closeHandler = function (e) {
            if ($close.is(':visible')) {
                method.close();
                e.preventDefault();
            }
        };
        $overlay.click(closeHandler);
        $close.click(closeHandler);
        $(document).keydown(function (e) { if (e.keyCode == 27) { closeHandler(e); } }); // Catch ESC

        $('body').append($overlay, $modal);
    }


    // Open the modal
    method.show = function (options) {
        method.init();
        method.isOpen = true;

        // Set default settings
        settings = {
            content: 'insert content',
            method: 'fade',
            closable: true,
            callback: null,
            width: 'auto',
            height: 'auto',
            timeout: 0,
            onClose: null
        };

        // Extend settings
        if (typeof options == 'string') options = { content: options };
        $.extend(settings, options);
        method.content = $(settings.content);

        // If it has id, lets clone and remove that id and show
        if (method.content.attr('id')) {
            method.content = method.content.clone().removeAttr('id').show();
        }
        if (typeof (settings.format) === 'object') {
            method.content.html(StringUtil.format(method.content.html(), settings.format));
        }

        $content.empty().append(method.content);

        $modal.css({
            width: settings.width || 'auto',
            height: settings.height || 'auto'
        });
        method.center();

        $(window).bind('resize.modal', method.center);
        if (!$modal.is(':visible')) {
            $overlay.show();
            if (settings.method == 'fade') {
                $modal.fadeIn('fast');
            } else {
                $modal.show();
            }
        }
        $close.toggle(settings.closable);

        // Set timeout if any
        clearTimeout(timeoutHandler);
        if (settings.timeout > 0) {
            timeoutHandler = setTimeout("Popup.close();", settings.timeout);
        }
        method.content.find('input:not(:hidden):first').focus();
        return method.content;
    };

    method.showLoading = function (options) {
        var newOpt = {
            closable: false
        };
        if (typeof options !== 'object') {
            newOpt.content = '<div id="modal-loading">' + (options || 'Indlæser') + '</div>';
        } else {
            $.extend(newOpt, options);
            newOpt.content = '<div id="modal-loading">' + options.content + '</div>';
        }
        method.show(newOpt);
    }

    method.showSuccess = function (options) {
        var newOpt = {
            closable: true,
            timeout: 2000
        };
        if (typeof options !== 'object') {
            newOpt.content = '<div id="modal-success">' + options + '</div>';
        } else {
            $.extend(newOpt, options);
            newOpt.content = '<div id="modal-success">' + options.content + '</div>';
        }
        method.show(newOpt);
    }


    method.showError = function (options) {
        var newOpt = {
            closable: true,
            timeout: 2000
        };
        if (typeof options !== 'object') {
            newOpt.content = '<div id="modal-error">' + options + '</div>';
        } else {
            $.extend(newOpt, options);
            newOpt.content = '<div id="modal-error">' + options.content + '</div>';
        }
        method.show(newOpt);
    }
    method.showWarning = function (options) {
        var newOpt = {
            closable: true,
            timeout: 2000
        };
        if (typeof options !== 'object') {
            newOpt.content = '<div id="modal-warning">' + options + '</div>';
        } else {
            $.extend(newOpt, options);
            newOpt.content = '<div id="modal-warning">' + options.content + '</div>';
        }
        method.show(newOpt);
    }
    // Close the modal
    method.close = function () {
        if (settings.method == 'fade') {
            $overlay.fadeOut('fast');
            $modal.fadeOut('fast', function () {
                $content.empty();
                if (settings.onClose) settings.onClose();
            });
        } else {
            $modal.hide();
            $overlay.hide();
            $content.empty();
            if (settings.onClose) settings.onClose();
        }

        $(window).unbind('resize.modal');
        method.isOpen = false;
    };
    method.hide = method.close;


    // Exposed methods
    return method;
}(jQuery);

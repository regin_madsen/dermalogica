<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >

  <!--
  Description: ul/li based navigation. No features from admin implemented.
  Recommended settings:
  Fold out: True or False
  Upper menu: Dynamic or Static
  First level: > 0
  Last level: >= First level
  -->

  <xsl:output method="xml" omit-xml-declaration="yes" indent="yes"  encoding="utf-8" />
  <xsl:param name="html-content-type" />
  <xsl:template match="/NavigationTree">

    <xsl:if test="count(//Page) > 0">
      <ul class="left">
        <li class="">
          <xsl:attribute name="src"></xsl:attribute>
        </li>
        <xsl:apply-templates select="Page">
          <xsl:with-param name="depth" select="1"/>
        </xsl:apply-templates>
</ul>
    </xsl:if>

  </xsl:template>

  <xsl:template match="//Page">
    <xsl:param name="depth"/>
    <li>
    	<xsl:attribute name="class">
        <xsl:if test="@InPath='True'">active inpath </xsl:if>
        <xsl:if test="position() = 1"> firstitem </xsl:if>
        <xsl:if test="position() = count(//Page)"> lastitem </xsl:if>
        <xsl:if test="count(Page)"> has-dropdown </xsl:if>
        <xsl:if test="@Active='True'"> activeitem </xsl:if>
        <xsl:value-of select="@NavigationTag" disable-output-escaping="yes"/>
</xsl:attribute>

      <a>
        <xsl:if test="@Allowclick = 'True'">
        <xsl:attribute name="href"><xsl:value-of select="@FriendlyHref" disable-output-escaping="yes"/></xsl:attribute>
     </xsl:if>
        <xsl:value-of select="@MenuText" disable-output-escaping="yes"/>
      </a>
       <xsl:if test="count(Page)">
          <ul class="dropdown">
            <xsl:apply-templates select="Page">
              <xsl:with-param name="depth" select="$depth+1"/>
            </xsl:apply-templates>
            <xsl:if test="$depth = 1">
            <li class="label"><label><img><xsl:attribute name="src"><xsl:value-of select="@Image" disable-output-escaping="yes"/></xsl:attribute></img></label></li>
          </xsl:if>
          </ul>

        </xsl:if>

    </li>
  </xsl:template>

</xsl:stylesheet>

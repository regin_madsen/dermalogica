<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" omit-xml-declaration="yes" indent="no" encoding="utf-8"/>
  <xsl:param name="html-content-type"/>

  <xsl:template match="/NavigationTree" xml:space="preserve">
    <xsl:if test="count(//Page) > 0">
        <xsl:apply-templates select="Page"/>
    </xsl:if>
  </xsl:template>

  <xsl:template match="//Page">
    <li>
      <xsl:attribute name="class">
        help
      </xsl:attribute>
      <a>
		<xsl:attribute name="class">
		<xsl:if test="@InPath='True' and @Active='True'">sel</xsl:if>
        <xsl:if test="@InPath='True' and @Active!='True'">opn</xsl:if>
		</xsl:attribute>
        <xsl:attribute name="href">
          <xsl:value-of select="@Href" disable-output-escaping="yes"/>
        </xsl:attribute>
        <xsl:choose>
          <xsl:when test="@MouseOver!='' and @MenuText!='Webshop'">
            <xsl:value-of select="@MouseOver" disable-output-escaping="yes"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@MenuText" disable-output-escaping="yes"/>
          </xsl:otherwise>
        </xsl:choose>
      </a>
      <xsl:if test="count(Page)>0">
      </xsl:if>
    </li>
  </xsl:template>
</xsl:stylesheet>
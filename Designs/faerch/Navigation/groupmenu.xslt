<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" omit-xml-declaration="yes" indent="no" encoding="utf-8"/>
  <xsl:param name="html-content-type"/>

  <xsl:template match="/NavigationTree" xml:space="preserve">
    <xsl:choose>
      <xsl:when test="count(//Page[@InPath='True']/Page) &gt;= 1">

          <ul id="grouplist" class="groups nobull">
            <xsl:apply-templates select="Page[@InPath='True']/Page">
              <xsl:with-param name="depth" select="1"/>
            </xsl:apply-templates>
          </ul>

      </xsl:when>
      <xsl:otherwise>
        <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    
  </xsl:template>
  <xsl:template match="//Page">
    <xsl:param name="depth"/>
    <xsl:variable name="LongGroup">
      <xsl:value-of select="substring-after(@Href, 'GroupID=')"/>
    </xsl:variable>
    <xsl:variable name="GroupNo">
      <xsl:value-of select="substring-before($LongGroup, '-at-SHOP1-')"/>
    </xsl:variable>
    <li>
      <div class="pad">
        <a>
          <xsl:attribute name="href">
            <xsl:value-of select="@FriendlyHref" disable-output-escaping="yes"/>
          </xsl:attribute>
          <xsl:attribute name="title">
            <xsl:choose>
              <xsl:when test="@MouseOver!=''">
                <xsl:value-of select="@MouseOver" disable-output-escaping="yes"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="@MenuText" disable-output-escaping="yes"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <xsl:call-template name="ecomgroup"/>
          <span class="name">
            <xsl:choose>
              <xsl:when test="@MouseOver!=''">
                <xsl:value-of select="@MouseOver" disable-output-escaping="yes"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="@MenuText" disable-output-escaping="yes"/>
              </xsl:otherwise>
            </xsl:choose>
            
          </span>
        </a>
      </div>
    </li>
  </xsl:template>
  <xsl:template name="ecomgroup">
    <xsl:variable name="ecomgroup">
      <xsl:value-of select="substring-after(@Href, 'GroupID=')"/>
    </xsl:variable>
    <xsl:variable name="ecomgroupid">
      <xsl:value-of select="substring-before($ecomgroup, '-at-SHOP1')"/>
    </xsl:variable>
    <xsl:variable name="groupimg">
      <xsl:text disable-output-escaping="yes" xml:space="preserve">/Admin/Public/GetImage.ashx?Image=/Files/Images/Group/</xsl:text>
      <xsl:value-of select="$ecomgroupid" />
      <xsl:text disable-output-escaping="yes" xml:space="preserve">.jpg&amp;Width=55&amp;Height=55</xsl:text>
    </xsl:variable>
    <span class="image">
        <xsl:attribute name="style">
          <xsl:text disable-output-escaping="yes">background-image:url(</xsl:text><xsl:value-of select="$groupimg" disable-output-escaping="yes" />
          <xsl:text disable-output-escaping="yes">);</xsl:text>
        </xsl:attribute>
    </span>      
  </xsl:template>
</xsl:stylesheet>

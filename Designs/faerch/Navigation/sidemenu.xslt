<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" omit-xml-declaration="yes" indent="no" encoding="utf-8"/>
  <xsl:param name="html-content-type"/>

  <xsl:template match="/NavigationTree" xml:space="preserve">
    <xsl:choose>
      <xsl:when test="count(//Page[@InPath='True']/Page) &gt;= 1">
        <div class="box">
          <ul class="pad sidemenu">
            <xsl:apply-templates select="Page[@InPath='True']">
              <xsl:with-param name="depth" select="1"/>
            </xsl:apply-templates>
          </ul>
        </div>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    
  </xsl:template>
  <xsl:template match="//Page">
    <xsl:param name="depth"/>
    <li>
		<xsl:if test="position()=last()"><xsl:attribute name="class">last</xsl:attribute></xsl:if>
      <a>
        <xsl:if test="@InPath='True' and @Active='True'">
          <xsl:attribute name="id">sel</xsl:attribute>
        </xsl:if>
        <xsl:if test="@InPath='True' and @Active!='True'">
          <xsl:attribute name="id">opn</xsl:attribute>
        </xsl:if>
        
        <xsl:attribute name="href">
          <xsl:choose>
            <xsl:when test="@Allowclick='True'">
              <xsl:value-of select="@FriendlyHref" disable-output-escaping="yes"/>
              <xsl:if test="contains(@Href,'ID=38')">?groupid=reorder</xsl:if>
            </xsl:when>
            <xsl:otherwise>
              javascript:void();
            </xsl:otherwise>
          </xsl:choose>
          
        </xsl:attribute>
		<xsl:attribute name="class">
			<xsl:text disable-output-escaping="yes">level</xsl:text><xsl:value-of select="@RelativeLevel" />
      <xsl:if test="@Allowclick!='True'"> nohand</xsl:if>
		</xsl:attribute>
        
            <xsl:value-of select="@MenuText" disable-output-escaping="yes"/>
        
      </a>
      <xsl:if test="count(Page) and @InPath='True'">
        <ul class="M{@AbsoluteLevel} nobull">
          <xsl:apply-templates select="Page">
            <xsl:with-param name="depth" select="$depth+1"/>
          </xsl:apply-templates>
        </ul>
      </xsl:if>
    </li>
  </xsl:template>

</xsl:stylesheet>

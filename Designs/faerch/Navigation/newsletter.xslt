<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >

  <!--
	Description: ul/li based navigation. No features from admin implemented.
	Recommended settings:
	Fold out: True or False
	Upper menu: Dynamic or Static
	First level: > 0
	Last level: >= First level
	-->

  <xsl:output method="xml" omit-xml-declaration="yes" indent="no"  encoding="utf-8" />
  <xsl:param name="html-content-type" />
  <xsl:template match="/NavigationTree" xml:space="preserve">
				<table width="180" cellpadding="0" cellspacing="0" border="0" style="border-bottom:1px solid #ccc;font-size:13px;">
          <thead>
            <tr>
              <th style="padding:5px 10px;text-align:left;border-top:1px solid #cccccc;border-bottom:1px solid #ffffff;background:#eeeeee;">Sortiment</th>
            </tr>
          </thead>
          <tbody>
            <xsl:apply-templates select="Page[contains(@NavigationTag,'main')]/Page[contains(@Href,'GroupID')]">
              <xsl:sort select="@MenuText" order="ascending" />
              <xsl:with-param name="depth" select="1"/>
            </xsl:apply-templates>
          </tbody>
        </table>
  </xsl:template>
  <xsl:template match="//Page">
    <xsl:param name="depth"/>
    <tr>
      <td style="border-top:1px solid #ccc;padding:5px 10px;">
      <a>
        <xsl:attribute name="href">
          http://<xsl:value-of select="//Global.Request.Host" disable-output-escaping="yes"/><xsl:value-of select="@FriendlyHref" disable-output-escaping="yes"/>
        </xsl:attribute>
		    <xsl:attribute name="style">
			    text-decoration:none;color:#444;font-weight:bold;
		    </xsl:attribute>
        <xsl:value-of select="@MenuText" disable-output-escaping="yes"/>
      </a>
      </td>
    </tr>
  </xsl:template>

</xsl:stylesheet>

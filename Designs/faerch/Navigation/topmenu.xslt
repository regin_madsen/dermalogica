<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" omit-xml-declaration="yes" indent="no" encoding="utf-8"/>
  <xsl:param name="html-content-type"/>

  <xsl:template match="/NavigationTree" xml:space="preserve">
    <xsl:if test="count(//Page) > 0">
      <ul class="dropmenu">
        <xsl:apply-templates select="Page[contains(@NavigationTag, 'main')]"/>
      </ul>
    </xsl:if>
  </xsl:template>

  <xsl:template match="//Page">
    <xsl:param name="depth"/>
    <li>
      <xsl:attribute name="class">
        
        <xsl:if test="position()=last()"> last</xsl:if>
        
      </xsl:attribute>
      <a>
		<xsl:attribute name="class">
      <xsl:text disable-output-escaping="yes">level</xsl:text><xsl:value-of select="@RelativeLevel" disable-output-escaping="yes" />
      <xsl:if test="@InPath='True' and @Active='True'"> sel</xsl:if>
      <xsl:if test="@InPath='True' and @Active!='True'"> opn</xsl:if>
      <xsl:if test="@Allowclick!='True'"> nohand</xsl:if>
		</xsl:attribute>
        <xsl:attribute name="href">
          <xsl:choose>
            <xsl:when test="@Allowclick='True'">
              <xsl:value-of select="@Href" disable-output-escaping="yes"/>
            </xsl:when>
            <xsl:otherwise>javascript:void();</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:choose>
          <xsl:when test="count(Page)>0 and @RelativeLevel=1">
            <xsl:text disable-output-escaping="yes">&lt;span class="count"&gt;</xsl:text>
            <xsl:value-of select="@MenuText" disable-output-escaping="yes"/>
            <xsl:text disable-output-escaping="yes">&lt;/span&gt;</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@MenuText" disable-output-escaping="yes"/>
          </xsl:otherwise>
        </xsl:choose>
        
      </a>
      <xsl:if test="count(Page)>0 and @AbsoluteLevel='1'">
        <ul class="M{@AbsoluteLevel}">
          <xsl:apply-templates select="Page">
            <xsl:with-param name="depth" select="$depth+1"/>
          </xsl:apply-templates>
        </ul>
      </xsl:if>
    </li>
  </xsl:template>
</xsl:stylesheet>
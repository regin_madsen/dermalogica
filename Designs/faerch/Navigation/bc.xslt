<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" omit-xml-declaration="yes" indent="no" encoding="utf-8"/>
  <xsl:param name="html-content-type"/>
  <xsl:template match="/NavigationTree" xml:space="preserve">
    <div class="bc"><xsl:if test="count(//Page) &gt; 0"><a href="/" class="bcH"><span><xsl:value-of select="Page[contains(@NavigationTag,'home')]/@MenuText"/></span></a><xsl:apply-templates select="//Page[@InPath='True' and @ShowInSitemap='True']"><xsl:with-param name="depth" select="1"/></xsl:apply-templates></xsl:if></div>
  </xsl:template>

  <xsl:template match="//Page[@InPath='True']">
    <xsl:param name="depth"/>
    
    <xsl:if test="@NavigationTag!='main home'">
      <span class="arrow">
        <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
      </span>
      <xsl:choose>
        <xsl:when test="@Allowclick='True' and @Active!='True'">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="@FriendlyHref" disable-output-escaping="yes"/>
            </xsl:attribute>
            <span>
              <xsl:value-of select="@MenuText" disable-output-escaping="yes"/>
            </span>
          </a>
        </xsl:when>
        <xsl:when test="@Allowclick='True' and @Active='True'">
          <a>
            <xsl:attribute name="class">
              <xsl:text disable-output-escaping="yes">nohand</xsl:text>
              <xsl:if test="@Active='True'"> activeitem</xsl:if>
            </xsl:attribute>
            <xsl:attribute name="href">javascript:void();</xsl:attribute>
            <xsl:value-of select="@MenuText" disable-output-escaping="yes"/>
          </a>
        </xsl:when>
        <xsl:otherwise>
          <a>
            <xsl:attribute name="class">
              <xsl:text disable-output-escaping="yes">nohand</xsl:text>
              <xsl:if test="@Active='True'"> activeitem</xsl:if>
            </xsl:attribute>
            <xsl:attribute name="href">javascript:void();</xsl:attribute>
            <xsl:value-of select="@MenuText" disable-output-escaping="yes"/>
          </a>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
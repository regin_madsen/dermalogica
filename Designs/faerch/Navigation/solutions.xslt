<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" omit-xml-declaration="yes" indent="no" encoding="utf-8"/>
  <xsl:param name="html-content-type"/>

  <xsl:template match="/NavigationTree" xml:space="preserve">
      <div class="colorbox">
        <xsl:apply-templates select="Page[contains(@NavigationTag, 'solutions')]/Page"/>
      </div>
  </xsl:template>

  <xsl:template match="//Page"> 
    <a>
      <xsl:attribute name="class">pos<xsl:value-of select="position()" disable-output-escaping="yes"/>
        <xsl:if test="position()=1"> first</xsl:if>
      </xsl:attribute>
      <xsl:attribute name="href">
        <xsl:value-of select="@Href" disable-output-escaping="yes"/>
      </xsl:attribute>
      <span class="vam">
        <xsl:value-of select="@MouseOver" disable-output-escaping="yes"/>
      </span>
    </a>
  </xsl:template>
</xsl:stylesheet>
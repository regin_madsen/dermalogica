﻿/*
    // DOCUMENTATION of version 1.1.0
    Added support for looking after [div/td].before and [div/td].after elements, to enhance the ability to custom the design
    Added support for true/false argument in toggleMode() to force mode
    Added support for { button: $('.myButton') } argument in addEditButton
    Added support for defining viewmode and editmode in constructor to override default edit/back buttons


    // DOCUMENTATION of version 1.0.0

    // Create listmode
    $('#myList').listmode(); 

    // Adds a button that is usable in edit mode
    $('#myList').listmode('addEditButton', {
        title: 'Hit me',            // Button text
        classes: 'myClassName',     // Adds class to button (Default is 'edit-button')
        position: 'bottom',         // Defines location of button: bottom or top (Default is 'top')
        callback: function(obj) {   // Callback when button is clicked

            var ids = obj.ids;      // Contains an array of ids selected (data-id)
            var items = obj.items;  // All selected items; either selected tr or li
            
            obj.callback(true);     // Tells the listmode to go back to view-mode
        }
    });


    // Adds a button that is usable in edit mode
    $('#myList').listmode('addEditButton', {
        button: [jQuery element],   // Use jQuery selector, e.g. $('a#myEditButton')
        callback: function(obj) {   // Callback when button is clicked

            var ids = obj.ids;      // Contains an array of ids selected (data-id)
            var items = obj.items;  // All selected items; either selected tr or li
            
            obj.callback(true);     // Tells the listmode to go back to view-mode
        }
    });

    // Adds a button that is usable in edit mode
    $('#myList').listmode('addViewButton', {
        title: 'Hit me',            // Button text
        classes: 'myClassName',     // Adds class to button (Default is 'view-button')
        position: 'bottom',         // Defines location of button: bottom or top (Default is 'top')
        callback: function() { }   // Callback when button is clicked
    });

    // Change mode between view-mode and edit-mode
    //  arg = true forces edit mode
    //  arg = false forces view mode
    //  arg = undefined: toggles mode

    $('#myList').toggleMode(arg);
    
    // Subscribe to modechange
    $('#myList').toggleMode(function($container, isEditMode) { });

    // Add sorting
    $('#myList').addSorting({
        selector: 'data-fav-id', // Selector of id (Default is data-id)
        callback: function(ids) {
            var ids = obj.ids;      // Contains an array of ids selected (id)
        }
    });

    // Add edit click (e.g. usable for edit name)    
    $('#myList').listmode('addEditClick', {
        selector: 'a.selector', // Selector inside item (Default is 'a')
        callback: function(obj) {
            console.log(obj.id);    // Writes the id (data-id)
            console.log(obj.item);  // jQuery object of item (e.g. [a.selector])
        }
    }); 

    $('#favoriteList').listmode({
        edit: function(obj) {},     // Uses 'addEditClick'
        sort: function(obj) { },    // Uses 'addSorting' with defaultSettings.translations.sort as default text
        create: function(obj) { },   // Uses 'addViewButton' with defaultSettings.translations.create as default text
        remove: function(obj) { }   // Uses 'addEditButton' with defaultSettings.translations.remove as default text
        editmode: [jquery object],      // Overrides the edit button
        viewmode: [jquery object],      // Overrides the view button
    });

    
*/

(function ($) {

    var methods = {};

    methods.init = function (options) {
        return this.each(function () {
            var $this = $(this);
            var settings = $.extend({ onSorting: [], onModeChange: [], onCheckboxChange: [] }, $this.data('_listmode') || {}, options);
            var translations = $.extend($.fn.listmode.translations, settings.translations || {});

            var internal = {
                $items: $([]),
                $itemsPre: null,
                $itemsPost: null,
                $headers: $([]),
                $headersPre: null,
                $top: null,
                $bottom: null,
                selected: [],
                isEditMode: false

            };

            internal.$top = $('<div class="listmode-top"><div class="view">' + (options.viewmode ? '' : createButton('edit', translations.edit)) + '</div><div class="edit">' + (options.editmode ? '' : createButton('view', translations.view)) + '</div></div>').insertBefore($this);
            internal.$bottom = $('<div class="listmode-bottom"><div class="view"></div><div class="edit"></div></div>').insertAfter($this);

            $this.find('> tr,> tbody > tr,> li').each(function () {
                var $item = $(this);
                if ($item.is('li[data-id]')) {
                    if ($item.find('div.before').length == 0) {
                        $('<div class="before listmode-generated dNone"></div>').prependTo($item);
                    }
                    if ($item.find('div.after').length == 0) {
                        $('<div class="after listmode-generated dNone"></div>').appendTo($item);
                    }
                    internal.$items = internal.$items.add($item);
                } else if ($item.is('tr[data-id]')) {
                    if ($item.find('td.before').length == 0) {
                        $('<td class="before listmode-generated dNone"></td>').prependTo($item);
                    }
                    if ($item.find('td.after').length == 0) {
                        $('<td class="after listmode-generated dNone"></td>').appendTo($item);
                    }
                    internal.$items = internal.$items.add($item);
                } else if ($item.is('tr:not([data-id])')) {
                    $('<td class="before listmode-generated dNone"></td>').prependTo($item);
                    $('<td class="after listmode-generated dNone"></td>').appendTo($item);
                }
            });
            $this.find('> thead > tr').each(function () {
                var $item = $(this);
                if ($item.find('.before').length == 0) {
                    $('<th class="before listmode-generated dNone"></th>').prependTo($item);
                }
                if ($item.find('.after').length == 0) {
                    $('<th class="after listmode-generated dNone"></th>').appendTo($item);
                }
                internal.$headers = internal.$headers.add($item);
            });

            internal.$itemsPre = internal.$items.filter('[data-id]').find('.before');
            internal.$itemsPost = internal.$items.filter('[data-id]').find('.after');
            internal.$headersPre = internal.$headers.find('.before');

            internal.$top.children('div.edit').hide();
            internal.$bottom.children('div.edit').hide();
            $this.addClass('listmode');

            // Bindings
            internal.$top.find('input.edit, input.view').click(function () { methods.toggleMode.apply($this, arguments); });
            internal.$items.children('.edit').hide();

            $this.data('_listmode.settings', settings);
            $this.data('_listmode.internal', internal);

            // Add general listeners
            if (settings.create) methods.addViewButton.call($this, { title: translations.create, callback: settings.create });
            if (settings.remove) methods.addEditButton.call($this, { title: translations.remove, callback: settings.remove });
            if (settings.sort) methods.addSorting.call($this, settings.sort);
            if (settings.edit) methods.addEditClick.call($this, { callback: settings.edit });
            if(settings.editmode) settings.editmode.click(function() { methods.toggleMode.call($this, false);});
            if (settings.viewmode) settings.viewmode.click(function () { methods.toggleMode.call($this, true); });
        });
    };

    methods.toggleMode = function (callback) {
        return this.each(function () {
            var $this = $(this);
            var internal = $this.data('_listmode.internal');
            if (!internal) throw 'jQuery.listmode: Must run init first!';
            var settings = $this.data('_listmode.settings');

            // Append listeners
            if (typeof callback === 'function') {
                settings.onModeChange.push(callback);
            } else {
                if (typeof (callback) === 'boolean') {
                    internal.isEditMode = callback;
                } else {
                    internal.isEditMode = internal.$top.children('div.edit').is(':hidden');
                }

                internal.$top.children('div.view').toggle(!internal.isEditMode);
                internal.$top.children('div.edit').toggle(internal.isEditMode);
                internal.$bottom.children('div.view').toggle(!internal.isEditMode);
                internal.$bottom.children('div.edit').toggle(internal.isEditMode);
                $this.find('.before').toggleClass('dNone', !internal.isEditMode || internal.$itemsPre.filter(':first').children().length == 0);
                $this.find('.after').toggleClass('dNone', !internal.isEditMode || internal.$itemsPost.filter(':first').children().length == 0);

                // Reset selected
                internal.selected.length = 0;
                internal.$items.filter('.selected').each(function () {
                    var $item = $(this);
                    $item.find('input[type="checkbox"]').removeAttr('checked');
                    $item.removeClass('selected');
                });
                internal.$top.add(internal.$bottom).children('.edit').find('input[type="button"]').addClass('disabled');


                $this.toggleClass('listmode-edit', internal.isEditMode);

                // Call listeners
                for (var i = 0; i < settings.onModeChange.length; i++) {
                    settings.onModeChange[i]($this, internal.isEditMode);
                }
            }
        });
    };

    methods.addViewButton = function (options) {
        options = $.extend({
            classes: 'view-button',
            title: 'No title',
            position: 'top'
        }, options);
        return this.each(function () {
            var $this = $(this);
            var internal = $this.data('_listmode.internal');
            if (!internal) throw 'jQuery.listmode: Must run init first!';
            
            var $container = (options.position == 'top') ? internal.$top : internal.$bottom;
            $(createButton(options.classes, options.title))
                .appendTo($container.children('.view'))
                .click(options.callback);
        });
    };

    methods.addEditButton = function (options) {
        options = $.extend({
            classes: 'edit-button',
            position: 'top',
            button: null // If defined, this will override class, position and text
        }, options);
        return this.each(function () {
            var $this = $(this);
            var internal = $this.data('_listmode.internal');
            if (!internal) throw 'jQuery.listmode: Must run init first!';
            var settings = $this.data('_listmode.settings');

            // Add markers if not added allready
            if (!internal.isCheckboxesAdded) {
                internal.isCheckboxesAdded = true;

                // Item click
                var checkboxClick = function () {
                    var $this = $(this);
                    var $parent = $this.parents('[data-id]:first');
                    var id = $parent.attr('data-id');
                    if ($this.is(':checked')) {
                        internal.selected.push(id);
                        $parent.addClass('selected');
                    } else {
                        internal.selected.splice(internal.selected.indexOf(id), 1);
                        $parent.removeClass('selected');
                        internal.$headersPre.find('[type="checkbox"]').removeAttr('checked');
                    }

                    internal.$top.add(internal.$bottom).children('.edit').find('input[type="button"]').toggleClass('disabled', internal.selected.length == 0);
                    for (var i = 0; i < settings.onCheckboxChange.length; i++) {
                        settings.onCheckboxChange($this, internal.selected);
                    }
                }

                // Header click
                $('<input type="checkbox" />').appendTo(internal.$headersPre).click(function () {
                    var $checkbox = $(this);
                    var $items = internal.$itemsPre.find('[type="checkbox"]').filter($checkbox.is(':checked') ? ':not(:checked)' : ':checked');
                    if ($checkbox.is(':checked')) {
                        $items.attr('checked', 'checked');
                    } else {
                        $items.removeAttr('checked');
                    }
                    $items.each(function () { checkboxClick.call(this); });
                });

                // See if checkboxes is manually inserted, otherwise generate checkbox
                var checkboxes = internal.$itemsPre.find('input[type="checkbox"]');
                if (checkboxes.length > 0) {
                    checkboxes.click(checkboxClick);
                } else {
                    $('<input type="checkbox" />').appendTo(internal.$itemsPre).click(checkboxClick);
                }

            }

            // Callback
            var buttonClick = function () {
                var relevantItems = internal.$items.filter('.selected[data-id]');
                var ids = internal.selected;
                options.callback({
                    items: relevantItems,
                    ids: internal.selected,
                    callback: function (hide) {
                        if (hide) {
                            methods.toggleMode.apply($this);
                        }
                    }
                });
            }

            // Use manually inserted button instead
            if (options.button && typeof (options.button.click) === 'function') {
                options.button.click(buttonClick);
            } else {
                var $container = options.position == 'top' ? internal.$top : internal.$bottom;
                $(createButton(options.classes, options.title))
                    .appendTo($container.children('.edit'))
                    .click(buttonClick);
            };
        });
    };

    methods.addSorting = function (options) {
        if (typeof (options) === 'function') options = { callback: options }; // For support of previous work
        options = $.extend({
            selector: 'data-id'
        }, options);
        return this.each(function () {
            var $this = $(this);
            var internal = $this.data('_listmode.internal');
            if (!internal) throw 'jQuery.listmode: Must run init first!';
            var settings = $this.data('_listmode.settings');
            settings.onSorting.push(options.callback);
            // Add sorting if not added
            if (internal.$itemsPost.filter(':first').find('.handle').length == 0) {
                $('<div class="handle edit"></div>')
                       .appendTo(internal.$itemsPost);

                var $holder = $this.children('tbody');
                if ($holder.length == 0) $holder = $this;
                $holder.sortable({
                    axis: 'y', handle: '.handle', update: function () {
                        var ids = $.makeArray($this.find('> tr[' + options.selector + '],> tbody > tr[' + options.selector + '],> li[' + options.selector + ']').map(function () { return $(this).attr(options.selector); }));

                        for (var i = 0; i < settings.onSorting.length; i++) {
                            settings.onSorting[i](ids);
                        }
                    }
                });

            }
        });
    };


    methods.addEditClick = function (options) {
        options = $.extend({ selector: 'a' }, options);
        return this.each(function () {
            var $this = $(this);
            var internal = $this.data('_listmode.internal');
            if (!internal) throw 'jQuery.listmode: Must run init first!';
            var settings = $this.data('_listmode.settings');

            internal.$items.find(options.selector).click(function (e) {
                var $this = $(this);
                if (internal.isEditMode) {
                    options.callback({
                        id: $this.parents('[data-id]:first').attr('data-id'),
                        item: $this
                    });
                    e.preventDefault();
                }
            });
        });
    };

    // Helpers
    function createButton(name, title) {
        return '<input type="button" class="' + name + '" value="' + title + '" />';
    }


  $.fn.listmode = function( method ) {
    
    if ( methods[method] ) {
      return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof method === 'object' || ! method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error( 'Method ' +  method + ' does not exist on jQuery.listmode' );
    }    
  
  };

  $.fn.listmode.translations = {
        create: 'Opret',
        remove: 'Slet markerede',
        edit: 'Rediger',
        view: 'Tilbage'
  };

})( jQuery );

﻿/// <reference path="Utilities/AjaxUtil.js" />
/// <reference path="underscore-min.js" />
/// <reference path="Popup.js" />

var FavoriteList = (function ($, _) {
    var methods = {},
        translations = {
            buttonAddToFavorite: 'Tilføj til favoritliste',
            buttonRemoveFromFavorite: 'Fjern fra favoritliste',
            mustPickProductsFirst: 'Du skal først vælge produkter',
            favoriteListItemsRemoved: 'Favoritterne er fjernet',
            errorOccoured: 'Der er sket en fejl',
            removed: 'Fjernet',
            view: 'Tilbagez0r',
            saved: 'Gemt',
            saving: 'Gemmer'
        },
        settings = {},
        elem = {
            $productList: null
        };

    methods.init = function (options) {
        options = options || {};
        settings = $.extend(settings, options);
        translations = $.extend(translations, options.translations || {});

        elem.$productList = elem.$productList || $('#productList').listmode({
            translations: translations,
            viewmode: $('.gotoEditMode')
        });


        elem.$productList.listmode('addEditButton', {
            title: translations.view,
            classes: 'view',
            position: 'bottom',
            callback: function (obj) {
                obj.callback(true);
            }
        });

        elem.$productList.listmode('addEditButton', {
            title: translations.buttonAddToFavorite,
            classes: 'add-to-favorite',
            position: 'bottom',
            callback: addToFavorites
        });

        elem.$productList.listmode('addEditButton', {
            title: translations.buttonAddToFavorite,
            classes: 'add-to-favorite',
            position: 'top',
            callback: addToFavorites
        });




        // Hide elements on edit (e.g. price)
        var $hideWhenEdit = elem.$productList.find('.hideWhenEdit');
        elem.$productList.listmode('toggleMode', function ($container, isEdit) {
            $hideWhenEdit.toggle(!isEdit);
        });

        activateFavInfoTooltips(elem.$productList);
    };

    methods.initMenu = function (options) {
        options = options || {};
        settings = $.extend(settings, options);
        translations = $.extend(translations, options.translations || {});


        $('#favoriteList').listmode({
            sort: sort,
            create: create,
            remove: remove,
            edit: edit,
            translations: translations
        });

        elem.$productList = elem.$productList || $('#productList').listmode();
        elem.$productList.listmode('addEditButton', {
            title: translations.buttonRemoveFromFavorite,
            classes: 'remove-from-favorite',
            position: 'top',
            callback: function (obj) { removeFromFavorite(options.id, obj); }
        });
        elem.$productList.listmode('addEditButton', {
            title: translations.buttonRemoveFromFavorite,
            classes: 'remove-from-favorite',
            position: 'bottom',
            callback: function (obj) { removeFromFavorite(options.id, obj); }
        });

    };

    methods.addSingle = function (id) {
        addToFavorites({
            ids: [id],
            callback: function () {
                location.reload();
            }
        });
    };

    methods.addSingleToDefaultList = function(productKey) {
        AjaxUtil.ajax('/WebServices/FavoriteService.asmx/AddToDefaultFavoriteList', { 'products': [productKey] }, function (defaultList) {
            //Popup.showSuccess(translations.saved);
            window.location.reload();
        }, function () {
            Popup.showError('Der er sket en fejl');
        });
    };


    methods.removeSingle = function(productKey, listId) {

        var data = { 'productKey': productKey };

        var url = '/WebServices/FavoriteService.asmx/RemoveProductFromDefaultList';
        if (listId) {
            data.listId = listId;
            url = '/WebServices/FavoriteService.asmx/RemoveProductFromList';
        }

        // RemoveProductFromList
        AjaxUtil.ajax(url, data, function () {
            window.location.reload();
        },
        function () {
            // Error
            Popup.showError(translations.errorOccoured);
        });
    };
    
    

    function remove(obj) {
        var $context = Popup.show({ content: $('#removeFavoriteListConfirm') });
        var $ul = $context.find('ul.confirmList');
        $ul.html('');
        obj.items.each(function () {
            $ul.append('<li>' + $(this).find('a').text() + '</li>');
        });
        $context.find('.confirmButton').click(function () {
            Popup.showLoading(translations.removeFavoriteLists);
            AjaxUtil.ajax('/WebServices/FavoriteService.asmx/RemoveFavoriteList', { ids: obj.ids }, function () {
                // Success
                Popup.showSuccess(translations.removed);
                // Reload if id is current
                for (var i = 0; i < obj.ids.length; i++) {
                    if (obj.ids[i] == settings.id)
                        location.reload();
                }
                obj.items.remove();
                obj.callback(true);
            },
            function () {
                // Error
                Popup.showError(translations.errorOccoured);
            });
        });
    }

    function edit(obj) {
        if (!Popup.isOpen) {
            var context = Popup.show({ content: $('#editFavoritList').html() });

            context.find('input[type="text"]')
                .keydown(function (e) { if (e.keyCode == 13) { edit(obj); } })
                .val($.trim(obj.item.text()))
                .focus();

            context.find('.editButton').click(function () { edit(obj); });

        } else {
            var text = Popup.content.find('input[type="text"]');
            var textVal = $.trim(text.val());
            if (textVal.length == 0) {
                text.next().show();
            } else {
                Popup.showLoading(translations.saving);
                AjaxUtil.ajax('/WebServices/FavoriteService.asmx/UpdateFavoritListName', { id: obj.id, name: textVal }, function () {
                    obj.item.html(textVal);
                    Popup.hide();
                },
                function () {
                    Popup.showError(translations.errorOccoured);
                });
            }
        }
    }

    function sort(ids) {
        var success = true;
        AjaxUtil.ajaxSync('/WebServices/FavoriteService.asmx/FavoriteListSortOrder', { order: ids }, function () {
            success = true;
        },
        function () {
            success = false;
        });
        return success;
    }

    function create(callback) {
        if (!Popup.isOpen) {
            var html = $('#createFavoritList');
            var content = Popup.show({ content: html });
            content.find('input[type="text"]')
                .keydown(function (e) { if (e.keyCode == 13) { create(callback); } })
                .focus();

            content.find('.createButton').click(function () { create(callback); });

        } else {

            var text = Popup.content.find('input[type="text"]');
            var textVal = $.trim(text.val());
            if (textVal.length == 0) {
                text.next().show();
            } else {
                Popup.showLoading(translations.creating);
                AjaxUtil.ajax('/WebServices/FavoriteService.asmx/CreateFavoriteList', { name: textVal }, function () {
                    location.reload();
                },
                function () {
                    Popup.showError(translations.errorOccoured);
                });
            }
        }
    }


    function addToFavorites(obj) {
        // Validate that at least one product is selected
        if (obj.ids.length == 0) {
            Popup.showError(translations.mustPickProductsFirst);
            return;
        }
        Popup.showLoading();
        AjaxUtil.ajax('/WebServices/FavoriteService.asmx/LoadFavoriteLists', {}, function (data) {
            $container = Popup.show({ content: $('#addToFavorites') });
            var $ul = $container.find('ul:first');
            for (var i = data.length - 1; i >= 0; i--) {
                $ul.prepend(StringUtil.format('<li><input type="checkbox" value="{id}" id="FavoriteList_{id}" /><label for="FavoriteList_{id}">{name} ({ItemCount})</label></li>', data[i]));
            }


            var $noneSelectedError = $container.find('.noneSelectedError');
            var $new = $container.find('.new:first');
            var $newCheckbox = $new.find('[type="checkbox"]');
            var $newError = $new.find('.error:first');
            var $newText = $new.find('[type="text"]:first').focus(function () { $newError.hide(); });
            var $checkboxes = $container.find('[type="checkbox"]').click(function () { if ($(this).is(':checked')) { $noneSelectedError.hide(); } });

            if (typeof (favoriteListId) === 'number') { // Disable because it is allready on it
                $('[type="checkbox"][value="' + favoriteListId + '"]').attr('disabled', 'disabled');
            }

            $container.find('.count').html(obj.ids.length);

            // New logic
            $newCheckbox.click(function () {
                var $this = $(this);
                if ($this.is(':checked')) {
                    $newText.removeAttr('disabled').focus();
                } else {
                    $newText.attr('disabled', 'disabled').val('');
                    $newError.hide();
                }
            });


            $container.find('input.submit:first').click(function () {
                if ($newCheckbox.is(':checked') && $.trim($newText.val()) == '') {
                    $newError.show();
                } else {
                    var data = {
                        favoriteLists: $.makeArray($container.find('[type="checkbox"]:checked').map(function (i, item) { var $item = $(item); return $item.val() == '' ? $item.next().val() : parseInt($item.val()); })),
                        products: $.makeArray($(obj.ids).map(function (i, item) { var is = item.split('|'); return { Id: is[0], VariantId: is[1], LanguageId: is[2] }; }))
                    };
                    if (data.favoriteLists.length == 0) {
                        $noneSelectedError.show();
                    } else {
                        Popup.showLoading('Gemmer');
                        AjaxUtil.ajax('/WebServices/FavoriteService.asmx/AddToFavoriteLists', data, function (result) {
                            if (obj.callback) obj.callback(true);
                            updateFavoriteLists(result);
                            if (obj.items) obj.items.find('.tooltipFav').removeClass('dNone');
                            activateFavInfoTooltips(elem.$productList);
                            Popup.showSuccess(translations.saved);
                        }, function () {
                            Popup.showError('Der er sket en fejl');
                        });
                    }
                }
                return false;
            });

        }, function (error) {
            Popup.showError("Der skete en fejl");
            console.log(error);
        });
    }


    function removeFromFavorite(id, obj) {

        // Validate that at least one product is selected
        if (obj.ids.length == 0) {
            Popup.showError(translations.mustPickProductsFirst);
            return;
        }
        var content = $('#confirmDeleteFavoriteItems');
        if (content.length == 0) throw 'Could not find confirm delete modal';
        content = Popup.show({ content: content });
        content.find('.count').html(obj.ids.length);

        content.find('input.confirm').click(function () {
            Popup.showLoading('Fjerner produkter fra favoritliste');
            var data = { id: id, ids: $.makeArray(obj.items.map(function (i, item) { return parseInt($(item).attr('data-fav-id')); })) };
            AjaxUtil.ajax('/WebServices/FavoriteService.asmx/RemoveFavoriteListItems', data, function (result) {
                obj.callback(true);
                obj.items.remove();
                updateFavoriteLists(result);
                $('#productlist').find('.tr0,.tr1').each(function (i) { $(this).removeClass('tr0').removeClass('tr1').addClass(i % 2 ? 'tr0' : 'tr1'); });
                Popup.showSuccess(translations.favoriteListItemsRemoved);
            }, function () {
                Popup.showError(translations.errorOccoured);
            });
        });

    }

    /* Private favorite lists */
    function updateFavoriteLists(data) {
        var $favlist = $('#favoriteList');
        if ($favlist.length == 0) return;
        for (var i = 0; i < data.length; i++) {
            $favlist.find('[data-id="' + data[i].Id + '"] .count').html(data[i].Count);
        }
    }

    function activateFavInfoTooltips($content) {
        if (!$content) return;

        $content.find('.tooltipFav:not(.dNone)').each(function () {
            var $this = $(this);
            if ($this.data('_tooltipFav')) return;

            $this.tooltip({
                position: {
                    my: "right-10 center",
                    at: "left center"
                },
                content: function () {
                    var $this = $(this);
                    // Cache favorite div to ensure title is available
                    var $div = $this.data('favDiv');
                    if (!$div) {
                        $div = $('<div class="favTooltip"><h4>' + $this.attr('title') + '</h4><ul><li class="loading"></li></ul></div>').width(200);
                        $this.data('favDiv', $div);
                    }
                    var $ul = $div.find('ul:first');
                    var productInfo = $this.parents('[data-id]:first').attr('data-id').split('|');
                    AjaxUtil.ajax('/WebServices/FavoriteService.asmx/GetProductFavoriteLists', { product: { Id: productInfo[0], LanguageID: productInfo[2] } }, function (data) {
                        $ul.html('');
                        for (var i = 0; i < data.length; i++) {
                            $ul.append(StringUtil.format('<li>{name}</li>', data[i]));
                        }
                    });
                    return $div;
                },
                items: '.tooltipFav:not(.dNone)'
            });

            $this.data('_tooltipFav', true);
        });

    }


    return methods;
}(jQuery, window._));
﻿/// <reference path="Utilities/AjaxUtil.js" />
/// <reference path="jquery.blockUI.js" />

var Cart = (function($) {

    var methods = {},
        elems = {},
        cartId = '',
        formId = '',
        blockOptions = {};

    var forceRecalculation = function (callback) {
        AjaxUtil.ajax('/Webservices/BasketService.asmx/ForcePriceRecalculation', function (result) {
            callback(result);
        });
    };

    methods.init = function (options) {

        cartId = options.cartId;
        formId = options.formId;

        blockOptions = options.blockOptions;
        elems.$cartTable = $('#' + options.cartId);
        elems.$form = $('#' + options.formId);
    };

    methods.updateCart = function () {

        forceRecalculation(function() {
            // get all input fields in the form, whose name does not start with 'CartV2.GotoStep'
            // this will get all relevant values of the form, and discard the one that redirects to the next step
            var formValues = $('#' + formId + " input:not([name^='CartV2.GotoStep'])").serialize();

            elems.$cartTable.block(blockOptions);
            $.post(elems.$form.attr('action'), formValues, function (d) {
                elems.$cartTable.html($(d).find('#' + cartId).html());
                elems.$cartTable.unblock();
            });
            
        });
    };


    return methods;

})(jQuery);
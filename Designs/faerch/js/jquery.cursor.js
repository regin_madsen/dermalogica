﻿// Developed by sml@norriq.com
(function ($) {
    function setSelection(input, options) {
        var _options = { start: 0, end: 0 };
        $.extend(_options, options);

        if (input.createTextRange) {
            var range = input.createTextRange();
            range.collapse(true);
            range.moveEnd('character', _options.start);
            range.moveStart('character', _options.end);
            range.select();
        } else if (input.setSelectionRange) {
            input.focus();
            input.setSelectionRange(_options.start, _options.end);
        }
    }

    $.fn.cursor = function (options) {
        return this.each(function () {
            var $this = $(this);
            var val = $this.val();
            if (val == '') {
                $this.focus();
            } else if (typeof options === 'number' || typeof options === 'undefined') {
                var max = $this.val().length;
                if (max == 0) {
                    return $this;
                }
                if (typeof position !== "number" || max < options)
                    options = max;

                setSelection($this[0], { start: options, end: options });
                $this.focus();
                return $this;
            } else {
                setSelection($this[0], options);
                $this.focus();
            }
        });
    }
})(jQuery);
﻿var DateUtil = (function(){
    var methods = {};

    methods.parseJsonDate = function (jsonDate) {
        return new Date(parseInt(jsonDate.replace('/Date(', '')));
    };

    methods.getEasterSunday = function (year) {
        if (typeof (year) === 'undefined') {
            year = new Date().getFullYear();
        } else if (typeof (year.getFullYear) === 'function') { // Date
            year = year.getFullYear();
        }
        var k = parseInt(year / 100);
        var x = parseInt((k - 17) / 25);
        var p = parseInt((k - x) / 3);
        var q = parseInt(k / 4);
        var M = parseInt((15 + k - p - q) % 30);
        var N = parseInt((4 + k - q) % 7);

        var a = parseInt(year % 19);
        var b = parseInt(year % 4);
        var c = parseInt(year % 7);
        var d = parseInt((19 * a + M) % 30);
        var e = parseInt((2 * b + 4 * c + 6 * d + N) % 7);

        var day = 22 + d + e;
        var month = 3;

        if (day > 31) {
            day = d + e - 9;
            month = 4;
            if (day == 26) {
                day = 19;
            }
            else if (d == 28 && e == 6 && parseInt((11 * M + 11) % 30) < 19 && day == 25) {
                day = 18;
            }
        }

        return new Date(year, month - 1, day);
    };

    methods.addDays = function (date, days) {
        return new Date(date.getFullYear(), date.getMonth(), date.getDate() + days, date.getHours(), date.getMinutes(), date.getSeconds(), date.getMilliseconds());
    };

    //6 torsdage efter skærtorsdag
    methods.getAscensionDay = function (easter) {
        var thursday = 0;
        var testDate = DateUtil.addDays(easter, -2);
        for (var ascension = 0; ascension < 50; ascension++) {

            if (testDate.getDay() == 4) {
                thursday += 1;
            }

            if (thursday == 6) {
                break;
            }

            testDate = DateUtil.addDays(testDate, 1);
        }
        return testDate;
    };

    methods.getPentecost = function (easter) {
        var sundays = 0;
        var Pentecost = DateUtil.addDays(easter, 1);
        for (var ascension = 0; ascension < 50; ascension++) {

            if (Pentecost.getDay() == 0) {
                sundays += 1;
            }

            if (sundays == 7) break;

            Pentecost = DateUtil.addDays(Pentecost, 1);
        }
        return Pentecost;
    };

    methods.getStoreBededag = function (easter) {
        var fridays = 0;
        var storebededag = DateUtil.addDays(easter, 1);
        for (var ascension = 0; ascension < 50; ascension++) {
            if (storebededag.getDay() == 5) {
                fridays += 1;
            }

            if (fridays == 4) break;

            storebededag = DateUtil.addDays(storebededag, 1);
        }
        return storebededag;
    };

    methods.getEaster2 = function (year) {
        if (typeof (year) === 'undefined') {
            year = new Date().getFullYear();
        } else if (typeof (year.getFullYear) === 'function') { // Date
            year == year.getFullYear();
        }
        var Y = year;

        var C = Math.floor(Y / 100);
        var N = Y - 19 * Math.floor(Y / 19);
        var K = Math.floor((C - 17) / 25);
        var I = C - Math.floor(C / 4) - Math.floor((C - K) / 3) + 19 * N + 15;
        I = I - 30 * Math.floor((I / 30));
        I = I - Math.floor(I / 28) * (1 - Math.floor(I / 28) * Math.floor(29 / (I + 1)) * Math.floor((21 - N) / 11));
        var J = Y + Math.floor(Y / 4) + I + 2 - C + Math.floor(C / 4);
        J = J - 7 * Math.floor(J / 7);
        var L = I - J;
        var M = 3 + Math.floor((L + 40) / 44);
        var D = L + 28 - 31 * Math.floor(M / 4);

        return new Date(year, M, D);
    };
    return methods;
}());
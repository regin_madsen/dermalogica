﻿/// <reference path="json2.js" />
/// <reference path="../jquery-1.9.0.min.js" />

var AjaxUtil = (function ($) {
    var methods = {};

    var log = function(msg) {
        console.log(msg);
    };

    methods.ajax = function(url, data, callback, errorCallback) {
        // Ensure data is in the correct format
        if (typeof data === 'object') {
            data = JSON.stringify(data);
        }

        return $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            url: url,
            data: data,
            success: function(result) {
                if (callback) {
                    callback(result.d);
                } else {
                    log(result.d);
                }
            },
            error: function(error) {
                if (errorCallback) {
                    errorCallback(error);
                } else {
                    log('ajax error!');
                }
            }
        });
    };

    methods.post = function(url, data, callback, errorCallback) {
        return $.ajax({
            url: url,
            data: data,
            dataType: 'html',
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            success: function(result) {
                var $result = $(result);
                var $content = $(result).filter('#agContent');

                result = $content.children(':first').html();

                // Following is for DwTemplateTags :)
                $('body').append($result.filter(':gt(' + $content.index() + ')').filter('div,table'));

                // Do the callback dance!
                if (callback) {
                    callback(result);
                } else {
                    log(result);
                }
            },
            error: function(error) {
                if (errorCallback) {
                    errorCallback(error);
                } else {
                    log('post error!');
                }
            }
        });
    };

    methods.throttle = function(callback, delay) {
        var timer = null;
        return function () {
            var context = this, args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
                callback.apply(context, args);
            }, delay);
        };
    };

    return methods;
}(jQuery));
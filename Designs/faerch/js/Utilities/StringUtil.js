﻿var StringUtil = (function () {
    var methods = {};

    methods.format = function (str, obj) {
        var type = typeof (str);
        if (type == 'object' && typeof (jQuery) !== 'undefined' && !jQuery.isPlainObject(str))
            type = 'jquery';

        // Convert to string
        switch (type) {
            case 'object':
                throw 'Cannot parse object unless it is jQuery';
                break;
            case 'jquery':
                str = jQuery('<div></div>').append(str.clone()).html();
                break;
        }

        // Replacement
        for (var item in obj) {
            var reg = RegExp('{-?' + item + '}', 'gi');
            str = str.replace(reg, obj[item] === undefined ? '' : obj[item]);
        }


        // Convert back
        switch (type) {
            case 'jquery':
                return $(str);
            default:
                return str;
        }
    };

    return methods;
}());
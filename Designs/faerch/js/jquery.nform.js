﻿/// <reference path="jquery.cursor.js" />

// Developed by sml@norriq.com
// Dependencies: jQuery.cursor
(function ($) {

    var methods = {};
    methods.init = function (options) {
        var settings = $.extend($.fn.nform.defaultSettings, options || {});
        if(settings.tabular && this.length > 1)
            methods.enableTab.apply(this, arguments);

        if (settings.quantity)
            methods.enableQuantity.apply(this, arguments);



        return this.each(function() {
            var $this = $(this);

        });
    };

    methods.enableQuantity = function(options) {
        var settings = $.extend($.fn.nform.defaultSettings, options || {});
        var $bundle = this.filter('[type="text"]');
        $bundle.keydown(function (e) {
            var $this = $(this);
            if (!settings.arrowKeysReverse && (e.keyCode == 37 || e.keyCode == 39) || settings.arrowKeysReverse && (e.keyCode == 40 || e.keyCode == 38)) { // Add or substract
                var val = parseInt($this.val());
                if (isNaN(val) || val < 0) val = 0;

                if ((e.keyCode == 37 || e.keyCode == 38) && val > 0) {
                    $this.val(val - 1);
                } else if (e.keyCode == 39 || e.keyCode == 40) {
                    $this.val(val + 1);
                }
                if (settings.triggerQuantityInstant) {
                    $this.trigger('change');
                } else {
                    // Delayed trigger
                    $this.data('triggerChange', true);
                }
                e.preventDefault();
            }
        });

        return this;
    };

    methods.enableTab = function(options) {
        var settings = $.extend($.fn.nform.defaultSettings, options || {});
        var $bundle = this.filter('[type="text"]');

        $bundle.each(function () {
            var $this = $(this);

            // Only take text fields
            if(!$this.is('[type="text"]')) return;

            $this.keydown(function(e) {
                // trigger change
                if (e.keyCode == 9 || settings.mapArrowKeys && (settings.arrowKeysReverse && (e.keyCode == 37 || e.keyCode == 39) || !settings.arrowKeysReverse && (e.keyCode == 40 || e.keyCode == 38))) {  // check up down
                    var newIndex = $bundle.index($this) + ((e.keyCode == 37 || e.keyCode == 38 || (e.keyCode == 9 && e.shiftKey)) ? -1 : 1);

                   
                    // trigger change
                    if ($this.data('triggerChange')) {
                        $this.trigger('change');
                        $this.data('triggerChange', null);
                    }

                    // Check if top or bottom
                    if (newIndex == -1) newIndex = $bundle.length - 1;
                    if (newIndex == $bundle.length) newIndex = 0;
                    $($bundle.get(newIndex)).cursor();
                    e.preventDefault();
                }
            });

        });

        return this;
    };



    $.fn.nform = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.nform');
        }
    };

    $.fn.nform.defaultSettings = {
        tabular: true,
        mapArrowKeys: true,
        arrowKeysReverse: false,
        quantity: false,
        triggerQuantityInstant: false
    };

})(jQuery);


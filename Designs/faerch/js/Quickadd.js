﻿/// <reference path="Utilities/AjaxUtil.js" />
/// <reference path="jquery.nform.js" />

var Quickadd = (function ($) {
    var methods = {},
        translations = {
            errorOccoured: 'Der er sket en fejl',
            addingToBasket: 'Tilføjer produkter til kurven',
            productsAdded: 'Tilføjet',
            removeFromList: 'Fjern produkt fra ekspresskøbliste'
        },
        elem = {},
        $productId,
        $quantity,
        prevValue = '',
        product = null,
        ajaxCall = null,
        multiadd = false; // is it possible to add multiple products at the same time

    methods.init = function (options) {
        if (typeof (options) === 'object') {
            translations = $.extend(translations, options.translations || {});

            if (options.multiadd) {
                multiadd = options.multiadd;
            }
        }

        var $content = $('#quickAdd').submit(methods.add);
        var $input = $content.find('input[type="text"]');

        elem = {
            $productId: $('#quickAddItemNo'),
            $quantity: $('#quickAddAmount'),
            $noproductFound: $('#quickAddNoproductFound').hide(),
            $productFound: $('#quickAddProductFound').hide(),
            $list: $('#quickAddList').hide(),
            $submit: $('#quickAdd input[type="submit"]').click(methods.submit)
        };
        elem.$productId.keyup(AjaxUtil.throttle(searchForItems, 250));
        $input.keydown(function (e) {
            if (e.keyCode == 13) { // 13=ENTER
                if (multiadd) {
                    methods.add();
                } else {
                    methods.submit();
                }
                
                e.preventDefault();
            }
        });
        
        $input.nform();
        updateTable(true);
    };


    methods.add = function () {
        if (!product) {
            return;
        }

        var productId = $.trim(elem.$productId.val());
        var quantity = parseInt(elem.$quantity.val()) || 1;
        if (quantity <= 0) {
            quantity = 1;
        }


        // Add row
        var $tr = $('<tr><td class="name"></td><td class="quantity"></td><td class="remove"><img src="/Files/Templates/NORRIQ_QuickAdd/minus-red.png" alt="remove" /></td></tr>')
            .attr('data-productId', product.Id)
            .attr('data-productVariantId', product.VariantId)
            .attr('data-productLanguageId', product.LanguageId)
            .attr('data-quantity', quantity);

        $tr.children('.name:first').html(product.Name).attr('title', 'ID: ' + product.Id);
        $tr.children('.quantity:first').html(quantity);
        $tr.find('.remove img').click(methods.remove).attr('title', translations.removeFromList);

        // Show table
        elem.$list.find('tbody').append($tr);
        if (multiadd) {
            elem.$list.show();

            // reset form
            updateTable();
            elem.$productId.focus();
        }
    };

    methods.remove = function () {
        var $this = $(this);
        $this.parents('tr:first').remove();
        updateTable();
        elem.$productId.focus();
    };




    methods.submit = function () {
        elem.$submit.attr('disabled', 'disabled');

        var productList = [];
        
        if (multiadd) {
            elem.$list.find('[data-productId]').each(function() {
                var $this = $(this);
                productList.push({
                    Id: $this.attr('data-productId'),
                    VariantId: $this.attr('data-productVariantId'),
                    LanguageId: $this.attr('data-productLanguageId'),
                    Quantity: parseInt($this.attr('data-quantity'))
                });
            });
        } else {
            var $p = elem.$productFound.find('.quickAddFoundName');
            var quantity = parseInt(elem.$quantity.val()) || 1;
            if (quantity <= 0) {
                quantity = 1;
            }

            productList.push({
                Id: $p.attr('data-productId'),
                VariantId: $p.attr('data-productVariantId'),
                LanguageId: $p.attr('data-productLanguageId'),
                Quantity: quantity
            });
        }

        

        if (productList.length > 0) {
            //showLoading(translations.addingToBasket);
            $.ajax({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                url: '/Webservices/BasketService.asmx/AddToBasket',
                data: JSON.stringify({ products: productList }),
                success: function(data) {
                    updateTable(true);
                    location.reload();
                },
                error: function() {
                    alert(translations.errorOccoured);
                }
            });
        }
    };


    function searchForItems() {
        var productId = $.trim(elem.$productId.val());
        if (prevValue == productId) return;
        prevValue = productId;

        setProduct(productId, null);

        if (ajaxCall) {
            ajaxCall.abort();
        }

        ajaxCall = $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify({ productId: productId }),
            url: '/Webservices/BasketService.asmx/FindProduct',
            success: function(data) {
                setProduct(productId, data.d);
            }
        });
    }


    function setProduct(productId, result) {
        product = result;
        elem.$productFound.toggle(product != null);
        elem.$noproductFound.toggle(product == null && productId != '');

        if (product) {
            elem.$productFound.find('.quickAddFoundName').html(product.Name);

            if (!multiadd) {
                elem.$productFound.find('.quickAddFoundName')
                    .attr('data-productId', product.Id)
                    .attr('data-productVariantId', product.VariantId)
                    .attr('data-productLanguageId', product.LanguageId);

                elem.$submit.removeAttr('disabled');
            }
        } else {
            if (!multiadd) {
                elem.$submit.attr('disabled', 'disabled');
            }
        }
    }
    
    

    function updateTable(clear) {
        if (clear) {
            elem.$list.find('tr[data-productId]').remove();
        }

        setProduct('', null);
        elem.$quantity.val('');
        elem.$productId.val('');

        var hasProducts = elem.$list.find('tr[data-productId]').length > 0;
        elem.$list.toggle(hasProducts);

        if (hasProducts) {
            elem.$submit.removeAttr('disabled');
        } else {
            elem.$submit.attr('disabled', 'disabled');
        }
    }

    function showUpdated() {
        // Copy pasta
        $("#itemAddedContainer").trigger('click');				
        window.setTimeout("closeFancy()", 1500);
    }

    return methods;

}(jQuery));

﻿/// <reference path="Utilities/AjaxUtil.js" />

var AlternativeDelivery = (function($) {

    var methods = {},
        elem = {},
        addresses = [];

    
    var populateSelector = function() {
        // use the webservice to get the alternative delivery addresses for the customer
        AjaxUtil.ajax('/WebServices/AlternativeDelivery.asmx/GetAlternativeDeliveryAddresses', {}, function (result) {
            if (result) {

                for (var i = 0; i < result.length; i++) {

                    // save the address in an array
                    var addr = result[i];
                    addresses[i] = addr;

                    // create an option tag, and store a reference to the address in the array
                    var option = $('<option />');
                    option.val(i);

                    // use values from the address to name the option
                    var displayName = "";
                    if (addr.EcomOrderDeliveryCompany) {
                        displayName = addr.EcomOrderDeliveryCompany;
                    } else if (addr.EcomOrderDeliveryName) {
                        displayName = addr.EcomOrderDeliveryName;
                    } else {
                        displayName = addr.EcomOrderDeliveryAddress;
                    }

                    option.html(addr.EcomOrderDeliveryZip + ' - ' + displayName);

                    elem.$select.append(option);
                }
            }
        });
    };

    var hook = function () {
        // hook up the change event to the selector
        elem.$select.change(function(e) {
            if (elem.$select.val() && elem.$select.val() != '') {

                // take the address from the array, based on the value of the option
                var addr = addresses[elem.$select.val()];
                for (var i in addr) {
                    // if the name starts with '__' then we do not need to handle it (the JS serializer in C# creates these)
                    if (i.indexOf('__') != 0) {
                        $('#' + i).val(addr[i]); // e.g. $('#EcomOrderDeliveryName').val('Anders');
                    }
                }
            }
        });
    };

    methods.init = function(options) {
        elem = {
            $select: $('#AlternativeDeliveryAddress')
        };

        populateSelector();
        hook();
    };


    return methods;
}(jQuery));